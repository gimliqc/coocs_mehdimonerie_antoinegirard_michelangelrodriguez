import sys
import connectionDB
import os
import util
import numpy as np
import argparse

class Analyse:
	def __init__(self, tailleFenetre, enc, ch):
		self.tailleFenetre = tailleFenetre
		self.enc = enc
		self.ch = ch
		self.synonyme = None
		self.nbSynonyme = 0
		self.methodeDeCalcul = 0
		self.reponse = None
		self.liste_de_mots = []
		self.dict_de_mots_unique = {}
		self.stop_list = util.getStopWords('stop-list.txt', "utf-8")
		self.insertMots = []
		self.insertCoocs = []
		self.updateCoocs = []
		#----------------------------------------------------------


	def parsing(self,dbd_mots):

		self.s = util.extraire_chaine(self.ch, self.enc)

		self.liste_de_mots = util.separer_mots(self.s)
		self.liste_de_mots_unique = util.separer_mots_unique(self.liste_de_mots)
		self.dict_de_mots_unique = util.list_to_dict_with_index(self.liste_de_mots_unique)
	
	def remplir_cooc(self, texte, tailleFenetre, dbd_mots):
		dbd_mots_swap = dict([(value, key) for key, value in dbd_mots.items()])
		tailleFenetre2 = tailleFenetre//2
		self.dictCoocs = {}
		for i in range(len(texte)):
			idMot = dbd_mots_swap[texte[i]]
			for j in range(1, tailleFenetre2 + 1):
				if i - j >= 0:
					idCooc = dbd_mots_swap[texte[i - j]]
					if idMot <= idCooc:
						if (idMot, idCooc) not in self.dictCoocs:
							self.dictCoocs[(idMot, idCooc)] = 1
						else:
							self.dictCoocs[(idMot, idCooc)] +=1
				if i + j < len(texte):
					idCooc = dbd_mots_swap[texte[i + j]]
					if idMot <= idCooc:
						if (idMot, idCooc) not in self.dictCoocs:
							self.dictCoocs[(idMot, idCooc)] = 1
						else:
							self.dictCoocs[(idMot, idCooc)] +=1

	def remplir_vecteurs(self, dbd_mots,  dbd_coocs):
		self.matrice = np.zeros( (len(dbd_mots), len(dbd_mots)) )
		for (idMot, idCooc,tFen), freq in dbd_coocs.items():
			self.matrice[idMot, idCooc] = freq
			self.matrice[idCooc, idMot] = freq

	
	def mots_to_insert(self,dbd_mots):
		index = len(dbd_mots)-1
		for mot in self.liste_de_mots_unique:
			if(mot) not in dbd_mots.values():
				index+=1
				dbd_mots[index] = mot
				self.insertMots.append((index, mot))


	def cooc_to_insert_and_update(self, dbd_cooc):
		for (idMot,idCooc),freq in self.dictCoocs.items():
			if(idMot,idCooc,self.tailleFenetre) in dbd_cooc:
				self.updateCoocs.append((freq+dbd_cooc[(idMot,idCooc,self.tailleFenetre)], idMot,idCooc,self.tailleFenetre))
			else: 
				self.insertCoocs.append((idMot,idCooc,self.tailleFenetre,freq))

	
	def remplir_matrice(self,texte, tailleFenetre, dictmot):
		tailleFenetre2 = tailleFenetre//2
		self.dictCoocs = {}
		for i in range(len(texte)):
			idMot = dictmot[texte[i]]
			for j in range(1, tailleFenetre2 + 1):
				if i - j >= 0:
					idCooc = dictmot[texte[i - j]]
					if (idMot, idCooc) not in self.dictCoocs:
						self.matrice[idMot, idCooc] = 1
					else:
						self.matrice[idMot, idCooc] +=1
				if i + j < len(texte):
					idCooc = dictmot[texte[i + j]]
					if (idMot, idCooc) not in self.dictCoocs:
						self.matrice[idMot, idCooc] = 1
					else:
						self.matrice[idMot, idCooc] +=1

	def calcul(self, dbd_mots):

		listeCalcul = []
		dbd_mots_swap = dict([(value, key) for key, value in dbd_mots.items()])
		indexSynonyme = dbd_mots_swap[self.synonyme]
		reverse = False
		if self.methodeDeCalcul == '0':
			reverse = True
			listeCalcul = self.attribuer_scores(np.dot, dbd_mots_swap)
		elif self.methodeDeCalcul == '1':
			listeCalcul = self.attribuer_scores(util.leastsquares, dbd_mots_swap)
		elif self.methodeDeCalcul == '2':
			listeCalcul = self.attribuer_scores(util.cityblock, dbd_mots_swap)

		listeCalculSorted = sorted(listeCalcul, reverse=reverse)

		for i in range(0, int(self.nbSynonyme)):
			print('%s --> %s' % (listeCalculSorted[i][1], listeCalculSorted[i][0]))
		
	def attribuer_scores(self, f, dbd_mots):
		scores = []
		indexSynonyme = dbd_mots[self.synonyme]
		v= self.matrice[indexSynonyme]
		for mot, index in dbd_mots.items():
			score = f(v, self.matrice[index])
			if mot not in self.stop_list and index != indexSynonyme:
				scores.append((score, mot))
		return scores