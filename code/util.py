from collections import Counter
import re
import numpy as np

def leastsquares(u, v):
	return np.sum((u-v)**2)

def cityblock(u, v):
	return np.sum(np.absolute(u-v))

def getStopWords(ch, enc):

	string = extraire_chaine(ch, enc)
	liste_de_mots = separer_mots(string)
	s = set(liste_de_mots)
	return s

def extraire_chaine(ch, enc):
	f = open(ch, 'r', encoding=enc)
	s = f.read()
	f.close()
	return s

def separer_mots(s):
	sTemp = re.findall('\w+', s.lower())
	return sTemp

def separer_mots_unique(l):
	return list(dict.fromkeys(l))

def list_to_dict_with_index(l):
	old_dict = dict(enumerate(l))
	new_dict = dict([(value, key) for key, value in old_dict.items()])
	return new_dict

def compter_lignes(s):
	return len(s.split('\n'))

def compter_caracteres(s):
	return len(s)

def compter_souschaine(s, sc):
	return s.count(sc)

def compter_mots(s):
	return len(s.split())

def compte_occurences_mots(s):
	listeDeMots = s.split()
	dictDeMots = dict(Counter(listeDeMots))
	return len(dictDeMots)

def most_common_word(s, n):
	listeDeMots = s.split()
	dictDeMots = dict(Counter(listeDeMots).most_common(n))
	return dictDeMots

def most_common_word_with_len(d):
	dictDeMotsWithLen = {}
	for value in d.values():
		dictDeMotsWithLen[value].append(len(value))

	print(dictDeMotsWithLen)

def longueurDesMots(s):
	listDeLongueurDeMots = []
	listeDeMots = s.split()
	for m in listeDeMots:
		listDeLongueurDeMots.append(len(m))