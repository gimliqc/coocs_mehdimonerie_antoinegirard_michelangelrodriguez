import cx_Oracle

class Connection:
	def __init__(self, tailleFenetre):
		self.connexion = self.connexion()
		self.cur = self.createCursor(self.connexion)
		self.tailleFenetre = tailleFenetre
		

	def fetch_dbd_mots(self):
		self.cur.execute('SELECT * FROM DICTMOTS')
		self.dbd_mots = dict(self.cur.fetchall())

		return self.dbd_mots


	def fetch_dbd_coocs(self):
		self.dbd_coocs = {}
		req = 'SELECT * FROM COOCURENCE WHERE tfen = :p'
		self.cur.execute(req,p=self.tailleFenetre)

		for a,b,c,d in self.cur.fetchall():
			x = a,b,c
			d = int(d)
			self.dbd_coocs[x] = d

		return self.dbd_coocs
	
	def remplirTableMots(self, insertMots):
		req1 = "insert into dictmots(id,mot) values (:1, :2)"
		self.cur.executemany(req1,insertMots)

		self.connexion.commit()
		
	def remplirTableCoocs(self, insertCooc):	
		req = "insert into coocurence(idmot, idcooc, tfen, freq) values (:1, :2, :3, :4)"
		self.cur.executemany(req,insertCooc)
		self.connexion.commit()
		
	def updateTableCoocs(self,updateCoocs):
		req = "update coocurence set freq = :1 where idmot = :2 and idcooc = :3 and tfen = :4"
		self.cur.executemany(req, updateCoocs)

		self.connexion.commit()

	
	def connexion(self): 
	    dsn_tns = cx_Oracle.makedsn('delta', 1521, 'decinfo')
	    chConnexion = 'e1689971/' + 'aaaAAA111' + '@' + dsn_tns

	    connex = cx_Oracle.connect(chConnexion)
	    return connex

	def createCursor(self, connexion):
	    cur = connexion.cursor()
	    return cur


	def closeConnexion(self, cur, connexion):
	    connexion.commit()
	    cur.close()
	    connexion.close()



