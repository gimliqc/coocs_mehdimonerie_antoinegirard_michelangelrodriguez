import analyse
import connectionDB
import sys
import util
import argparse
import time

desc ="\nEntrez un mot, le nombre de synonyme que vous voulez et la méthode de calcul,\ni.e. produit scalaire: 0, least-squares: 1, city-block: 2\n\nTapez q pour quitter.\n\n"

def main():
	parser = argparse.ArgumentParser()
	parser.add_argument("-e", help="utilise entrainement",
	action="store_true")
	parser.add_argument("-r", help="utilise recherche",
	action="store_true")
	parser.add_argument("--enc", help="spécifie l'encodage")
	parser.add_argument("-t", help="spécifie la taille de fenêtre",
		type=int)
	parser.add_argument("--chemin", help="spécifie le chemin")
	args = parser.parse_args()

	if args.e:
		startProgram = time.time()

		a = analyse.Analyse(args.t, args.enc, args.chemin)
		c = connectionDB.Connection(args.t)

		dbd_mots = c.fetch_dbd_mots()
		dbd_coocs = c.fetch_dbd_coocs()

		a.parsing(dbd_mots)
		
		a.mots_to_insert(dbd_mots)
		c.remplirTableMots(a.insertMots)
		a.remplir_cooc(a.liste_de_mots, a.tailleFenetre, dbd_mots)

		#a.remplir_vecteurs(dbd_mots)

		a.cooc_to_insert_and_update(dbd_coocs)
		
		c.remplirTableCoocs(a.insertCoocs)
		c.updateTableCoocs(a.updateCoocs)
		
		c.closeConnexion(c.cur, c.connexion)

		endProgram = time.time()
		print("\nTemps Total: %f" % (endProgram - startProgram))

	elif args.r:
		a = analyse.Analyse(args.t, "", "")
		c = connectionDB.Connection(args.t)

		dbd_mots = c.fetch_dbd_mots()
		dbd_coocs = c.fetch_dbd_coocs()

		a.remplir_vecteurs(dbd_mots, dbd_coocs)
		

		while a.reponse is not "q":
			a.reponse = input(desc)
			if a.reponse is "q":
				sys.exit()
			else:
				a.synonyme, a.nbSynonyme, a.methodeDeCalcul = a.reponse.split()
				print('\n')
				a.calcul(dbd_mots)
	return 0


if __name__ == '__main__':
	main()